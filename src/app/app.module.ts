import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';

import { AppComponent } from './app.component';
import { TitleComponent } from './home-3-1/title/title.component';
import { MessageComponent } from './home-3-1/message/message.component';
import { TimerComponent } from './home-3-1/timer/timer.component';
import { HomeThreeOneComponent } from './home-3-1/home-three-one.component'; 
import { AppRoutingModule } from './app-routing.module';
import { HomeThreeTwoComponent } from './home-3-2/home-three-two.component';
import { MoviesListComponent } from './home-3-2/movies-list/movies-list.component';
import { MoviesListItemComponent } from './home-3-2/movies-list/movies-list-item/movies-list-item.component';
import { ChangeDataComponent } from './home-3-2/change-data/change-data.component';


@NgModule({
  declarations: [
    AppComponent,
    TitleComponent,
    MessageComponent,
    TimerComponent,
    HomeThreeOneComponent,
    HomeThreeTwoComponent,
    MoviesListComponent,
    MoviesListItemComponent,
    ChangeDataComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent] 
})
export class AppModule { }
