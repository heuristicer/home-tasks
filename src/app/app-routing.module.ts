import { NgModule } from '@angular/core'; 
import { Routes, RouterModule } from '@angular/router';
import { HomeThreeOneComponent } from './home-3-1/home-three-one.component';
import { HomeThreeTwoComponent } from './home-3-2/home-three-two.component';




const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'home-3-1' },  
  { path: 'home-3-1', component: HomeThreeOneComponent }, 
  { path: 'home-3-2', component: HomeThreeTwoComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)], 
  exports: [RouterModule] 
}) 

export class AppRoutingModule { }