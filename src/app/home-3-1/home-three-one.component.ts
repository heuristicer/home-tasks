import { Component, OnInit, Input, Output } from '@angular/core';

@Component({
  selector: 'app-home-three-one',
  templateUrl: './home-three-one.component.html',
  styleUrls: ['./home-three-one.component.scss']
})
export class HomeThreeOneComponent implements OnInit {

  title: string = 'Hello, Child Component!';
  message: string = 'Timer is Static!';
  messageClass: string;  

  changeState(changedState) {
    this.message= changedState ? 'Timer is Running!': 'Timer is not Running!';
    this.messageClass = changedState;
  }

  constructor() { }

  ngOnInit() {
  }

}
