import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.scss']
})
export class TimerComponent implements OnInit {

  @Input() message: string;
  @Output() onRunning = new EventEmitter<boolean>(); 
  



  defaultValue: number = 10;
  timerValue: number = this.defaultValue;
  time: any;
  disabled: boolean = true;
  isShow: boolean = false;

  changeDisabled() {
    this.disabled = !this.disabled;
  }
  

  playTimer() {
    this.changeDisabled();
    this.onRunning.emit(true);
    this.time = setInterval(() => {
      if (this.timerValue > 0) {
        this.timerValue--;
      }
      else {
        clearInterval(this.time);
        this.changeDisabled();
        this.isShow = !this.isShow;
        this.onRunning.emit(false)
      }

    }, 1000)
  }

  pauseTimer() {
    this.changeDisabled();
    clearInterval(this.time);
    this.onRunning.emit(false)
  }

  resetTimer() {
    this.timerValue = this.defaultValue;
    this.isShow = !this.isShow;
  }


  constructor() { }

  ngOnInit() {
  }


}
