export class Movie {
    constructor(
        public imagePath: string,
        public title: string,
        public year: number, 
        public myRaiting: number,
        public genres: string[],
        public length: string,
        public review?: string,
        public imdbRaiting?: number,
        public trailer?: string
    ) { }
}
