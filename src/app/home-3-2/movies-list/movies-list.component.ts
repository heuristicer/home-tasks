import { Component, OnInit, Input } from '@angular/core';
import { Movie } from 'src/app/shared/models/moovie.model'

@Component({
  selector: 'app-movies-list',
  templateUrl: './movies-list.component.html',
  styleUrls: ['./movies-list.component.scss']
})
export class MoviesListComponent implements OnInit {

  @Input() data: boolean;

  movies: Movie[] = []; 
  movies2: Movie[] = []; 

  constructor() { }

  ngOnInit() { 
    this.movies = [
      new Movie(
        "https://m.media-amazon.com/images/M/MV5BNjQ3NWNlNmQtMTE5ZS00MDdmLTlkZjUtZTBlM2UxMGFiMTU3XkEyXkFqcGdeQXVyNjUwNzk3NDc@._V1_.jpg",
        "Harry Potter and the Sorcerer's Stone",
        2002,
        8.3,
        ['Fantasy', 'Adventure'],
        "2h 50m"
      ),
      new Movie(
        "https://m.media-amazon.com/images/M/MV5BNjQ3NWNlNmQtMTE5ZS00MDdmLTlkZjUtZTBlM2UxMGFiMTU3XkEyXkFqcGdeQXVyNjUwNzk3NDc@._V1_.jpg", 
        "Harry Potter and the Sorcerer's Stone",
        2002,
        8.3,
        ['Fantasy', 'Adventure'],
        "2h 50m"
      ),
      new Movie(
        "https://m.media-amazon.com/images/M/MV5BNjQ3NWNlNmQtMTE5ZS00MDdmLTlkZjUtZTBlM2UxMGFiMTU3XkEyXkFqcGdeQXVyNjUwNzk3NDc@._V1_.jpg",
        "Harry Potter and the Sorcerer's Stone",
        2002,
        8.3,
        ['Fantasy', 'Adventure'],
        "2h 10m"
      ),
      new Movie(
        "https://m.media-amazon.com/images/M/MV5BNjQ3NWNlNmQtMTE5ZS00MDdmLTlkZjUtZTBlM2UxMGFiMTU3XkEyXkFqcGdeQXVyNjUwNzk3NDc@._V1_.jpg",
        "Harry Potter and the Sorcerer's Stone",
        2001,
        8.3,
        ['Fantasy', 'Adventure'],  
        "2h 10m"
      )

    ]; 
    this.movies2 = [
      new Movie(
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR4OHpr8SjkaI90rKjTA0k7iOA_H-f__nARguU71Hh1N0lnzxb6&s",
        "Lord of The Rings. The Two Towers",
        2001,
        8.9,
        ['Fantasy', 'Adventure'],
        "2h 10m"
      ),
      new Movie(
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR4OHpr8SjkaI90rKjTA0k7iOA_H-f__nARguU71Hh1N0lnzxb6&s", 
        "Lord of The Rings. The Two Towers",
        2001,
        8.9,
        ['Fantasy', 'Adventure'],
        "2h 10m"
      ),
      new Movie(
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR4OHpr8SjkaI90rKjTA0k7iOA_H-f__nARguU71Hh1N0lnzxb6&s",
        "Lord of The Rings. The Two Towers",
        2001,
        8.9,
        ['Fantasy', 'Adventure'],
        "2h 50m"
      ),
      new Movie(
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR4OHpr8SjkaI90rKjTA0k7iOA_H-f__nARguU71Hh1N0lnzxb6&s",
        "Lord of The Rings. The Two Towers",
        2002,
        8.9,
        ['Fantasy', 'Adventure'],  
        "2h 50m"
      )

    ];
  }
}
