import { Component, OnInit, Input } from '@angular/core';
import { Movie } from 'src/app/shared/models/moovie.model'

@Component({
  selector: 'app-movies-list-item',
  templateUrl: './movies-list-item.component.html',
  styleUrls: ['./movies-list-item.component.scss']
})
export class MoviesListItemComponent implements OnInit {

  @Input() m: Movie

  constructor() { }

  ngOnInit() {
  }

}
