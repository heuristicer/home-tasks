import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home-three-two',
  templateUrl: './home-three-two.component.html',
  styleUrls: ['./home-three-two.component.scss']
})
export class HomeThreeTwoComponent implements OnInit {

  data:boolean;

  changedData(changes) {
    this.data = changes;
  }

  constructor() { } 

  ngOnInit() {
  }

}
