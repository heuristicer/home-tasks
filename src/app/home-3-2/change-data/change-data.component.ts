import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-change-data',
  templateUrl: './change-data.component.html',
  styleUrls: ['./change-data.component.scss']
})
export class ChangeDataComponent implements OnInit {

  @Output() onChanged = new EventEmitter<boolean>(); 

  data: boolean = true;

  changeData() {
    this.data = !this.data
    this.onChanged.emit(this.data);
  }

  constructor() { }

  ngOnInit() {
  }

}
